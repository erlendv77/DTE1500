package exception;

import javax.annotation.processing.FilerException;
import java.io.*;

public class FileNotFound {

	public static void main(String[] args) {


		try {
			readAndPrintFile("/tmp/text.txt");
		} catch (UnableToReadFileException e) {
			e.printStackTrace();
			Throwable cause = e.getCause();

			StackTraceElement[] stackTrace = e.getStackTrace();
			if (stackTrace.length > 0) {
				StackTraceElement source = stackTrace[0];
				System.out.printf("%s occured in %s.%s at line %d caused by %s\n",
						e.getMessage(),
						source.getClassName(),
						source.getMethodName(),
						source.getLineNumber(),
						cause.getClass().getName());
			}
		}
		System.out.println("Finished");

	}


	public static void readAndPrintFile(String filename) throws UnableToReadFileException{
		String data = readFile(filename);
		System.out.println("File read:");
		System.out.println(data);

		System.out.println("Finished");
	}

	public static String readFile(String filename) throws UnableToReadFileException {
		try {
			File file = new File(filename);

			String result = "";

			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
			for (String line = reader.readLine(); line != null; line = reader.readLine()) {
				result += line + "\n";
			}

			return result;
		} catch (IOException e) {
			throw new UnableToReadFileException("Unable to read file", e);
		}
	}



}
