package exception;

public class ObjectIsNotStringException extends Exception {
	public ObjectIsNotStringException(String message, Throwable cause) {
		super(message, cause);
	}

	public ObjectIsNotStringException(String message) {
		super(message);
	}
}
