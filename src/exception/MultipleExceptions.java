package exception;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class MultipleExceptions {


	public static void main(String[] args) {
		try {
			printObjectAsString(1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void printObjectAsString(Object o) throws ObjectIsNullException, ObjectIsNotStringException{
		try {
			System.out.println(((String) o).toString());
		} catch (NullPointerException e) {
			throw new ObjectIsNullException("Object is null", e);
		} catch (ClassCastException e) {
			throw new ObjectIsNotStringException("Object is not string", e);
		}
	}


}
