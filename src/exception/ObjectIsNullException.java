package exception;

public class ObjectIsNullException extends Exception {

	public ObjectIsNullException(String message, Throwable cause) {
		super(message, cause);
	}

	public ObjectIsNullException(String message) {
		super(message);
	}
}
