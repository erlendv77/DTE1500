package exception;

public class UnableToReadFileException extends Exception {



	public UnableToReadFileException(String message) {
		super(message);
	}

	public UnableToReadFileException(String message, Throwable cause) {
		super(message, cause);
	}
}
