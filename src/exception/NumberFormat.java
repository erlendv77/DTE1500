package exception;

public class NumberFormat {


	public static void main(String[] args) {


		try {
			Object o = null;
			System.out.println(o.toString());
		} catch (NullPointerException e) {
			System.out.println("Invalid reference: null");
		}
	}


	public static int parseInt(String sInt) {
		return Integer.parseInt(sInt);
	}

}
