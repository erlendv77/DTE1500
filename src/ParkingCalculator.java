class ParkingCalculator {

	public static void main(String[] args) {
		System.out.println(beregnAvgift(0));
		System.out.println(beregnAvgift(24));
		System.out.println(beregnAvgift(25));
		System.out.println(beregnAvgift(27));
		System.out.println(beregnAvgift(28));
		System.out.println(beregnAvgift(128));
	}

	static double beregnAvgift(int antallTimer) {
		System.out.println("Beregner avgift for " + antallTimer + " timer");
		int days = antallTimer / 24;
		int rest = (antallTimer - (days * 24));

		double price = 40;
		if (rest == 0) {
			price = 0;
		}

		if (rest > 3) {
			price += (rest - 3) * 10;
		}

		price = Math.min(price, 180);
		price += days * 180;

		return price;
	}
}