package shapes;

import java.awt.*;

public class AnotherClass implements GraphicalObject {
	@Override
	public void paint(Graphics graphics) {
		graphics.drawLine(100, 100, 200, 200);
	}
}
