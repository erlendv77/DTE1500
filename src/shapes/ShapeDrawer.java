package shapes;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class ShapeDrawer extends JComponent {

	private ArrayList<GraphicalObject> graphicalObjects = new ArrayList<GraphicalObject>();


	public void add(GraphicalObject graphicalObject) {
		graphicalObjects.add(graphicalObject);
	}

	@Override
	public void paint(Graphics g) {
		((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		for (int i = 0; i < graphicalObjects.size(); i++) {
			GraphicalObject  graphicalObject = graphicalObjects.get(i);
			graphicalObject.paint(g);
		}
	}
}
