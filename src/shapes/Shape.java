package shapes;

import java.awt.*;

public abstract class Shape implements GraphicalObject{
	protected int x;
	protected int y;

	public Shape(int x, int y) {
		this.x = x;
		this.y = y;
	}
}
