package shapes;

import java.awt.*;

public class Circle extends Shape {
	private int radius;

	public Circle(int x, int y, int radius) {
		super(x, y);
		this.radius = radius;
	}

	@Override
	public void paint(Graphics graphics) {
		graphics.drawOval(x, y, radius * 2, radius * 2);
	}
}
