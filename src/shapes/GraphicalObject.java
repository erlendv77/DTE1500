package shapes;

import java.awt.*;

public interface GraphicalObject {

	public void paint(Graphics graphics);

}
