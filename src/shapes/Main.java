package shapes;

import javax.swing.*;
import java.awt.event.ActionListener;

public class Main {


	public static void main(String[] args) {
		JFrame frame = new JFrame();
		ShapeDrawer shapeDrawer = new ShapeDrawer();




		shapeDrawer.add(new Square(10, 10, 100, 100));
		shapeDrawer.add(new Circle(10, 10, 100));
		shapeDrawer.add(new Circle(100, 50, 100));
		shapeDrawer.add(new AnotherClass());

		frame.add(shapeDrawer);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(1024, 768);
		frame.setVisible(true);
	}

}
