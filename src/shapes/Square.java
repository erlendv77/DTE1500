package shapes;

import java.awt.*;

public class Square extends Shape {
	private int width;
	private int height;


	public Square(int x, int y, int width, int height) {
		super(x, y);
		this.width = width;
		this.height = height;
	}

	public void paint(Graphics g) {
		g.drawRect(x, y, width, height);
	}
}
