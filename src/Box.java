import java.util.*;

/**
 * Created by erlendvalle on 16.11.2017.
 */
public class Box {
	private int width;
	private int height;
	private int length;

	public Box() throws Exception {
		this.setDimensions(1, 1, 1);
	}

	public Box(int width, int height, int length) throws Exception {
		this.setDimensions(width, height, length);
	}

	public void setDimensions(int width, int height, int length) throws Exception {
		this.width = this.returnCorrectDimension(width);
		this.height = this.returnCorrectDimension(height);
		this.length = this.returnCorrectDimension(length);
	}

	private int returnCorrectDimension(int value) throws Exception {
		if (value < 1 || value > 20) {
			throw new Exception("Value must be between 1 and 20");
		}

		if (value < 1) {
			return 1;
		} else if (value > 20) {
			return 20;
		} else {
			return value;
		}
	}



	public String toString() {
		return width + "x" + height + "x" + length;
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		boolean validNumber = false;
		while (!validNumber) {
			try {
				System.out.print("Enter a number: ");
				Integer.parseInt(scanner.nextLine());
				validNumber = true;
			} catch (NumberFormatException e) {
				System.out.println("Invalid number, please try again");
			}
		}

		try {

		} catch (Exception e) {
			System.out.println("You failed: " + e.getMessage());
		}

	}

}

