import java.util.Scanner;

public class RetailSellerCoupons {


	public static void main(String[] args) {
		int[][] salgsTabell = new int[5][4];
		int[] prisTabell = {200, 700, 2000, 5000};


		Scanner scanner = new Scanner(System.in);


		while (true) {
			System.out.print("Skriv inn selgernummer [1-5] (-1 avslutter): ");
			int selgerNummber = Integer.parseInt(scanner.nextLine());
			if (selgerNummber == -1) {
				break;
			}

			System.out.print("Skriv inn produktnummer [1-4]: ");
			int produktNummer = Integer.parseInt(scanner.nextLine());

			System.out.print("Skriv inn antall: ");
			int antall = Integer.parseInt(scanner.nextLine());

			salgsTabell[selgerNummber - 1][produktNummer - 1] += antall;
		}


		for (int a = 0; a < salgsTabell.length; a++) {
			int totalt = 0;
			for (int b = 0; b < salgsTabell[a].length; b++) {
				totalt += salgsTabell[a][b] * prisTabell[b];
			}
			System.out.println("Selger nummer " + (a + 1) + " : " + totalt);
		}
	}
}
