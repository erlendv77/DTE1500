package module4;

import java.util.Scanner;

/**
 * Created by erlendvalle on 13.09.2017.
 */
public class Assignment3 {
	public static void main(String[] args) {
		String word1;
		String word2;
		String word3;

		String firstWord;
		String secondWord;
		String thirdWord;

		Scanner scanner = new Scanner(System.in);

		System.out.print("Skriv inn første ord: ");
		word1 = scanner.nextLine();

		System.out.print("Skriv inn andre ord: ");
		word2 = scanner.nextLine();

		System.out.print("Skriv inn tredje ord: ");
		word3 = scanner.nextLine();


		if (word1.compareTo(word2) < 0) {
			if (word2.compareTo(word3) < 0) {
				firstWord = word1;
				secondWord = word2;
				thirdWord = word3;
			} else if (word1.compareTo(word3) < 0) {
				firstWord = word1;
				secondWord = word3;
				thirdWord = word2;
			}  else {
				firstWord = word3;
				secondWord = word1;
				thirdWord = word2;
			}
		} else if(word1.compareTo(word3) < 0) {
			if (word2.compareTo(word3) < 0) {
				firstWord = word2;
				secondWord = word1;
				thirdWord = word3;
			} else {
				firstWord = word2;
				secondWord = word1;
				thirdWord = word3;
			}
		} else {
			if (word2.compareTo(word3) < 0) {
				firstWord = word2;
				secondWord = word3;
				thirdWord = word1;
			} else {
				firstWord = word3;
				secondWord = word2;
				thirdWord = word1;
			}
		}

		System.out.println(firstWord + " " + secondWord + " " + thirdWord);


	}
}
