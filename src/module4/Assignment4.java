package module4;

import java.util.Scanner;

/**
 * Created by erlendvalle on 13.09.2017.
 */
public class Assignment4 {
	public static void main(String[] args) {
		String input;
		char strGrade;
		int intGrade;

		Scanner scanner = new Scanner(System.in);

		System.out.print("Oppgi en bokstavkarakter: ");

		input = scanner.nextLine();
		if (input.length() != 1) {
			System.err.println("Oppgi en enkelt karakter");
			System.exit(1);
		}

		strGrade = input.toUpperCase().charAt(0);
		if (strGrade < 'A' || strGrade > 'F') {
			System.err.println(strGrade + " er en ugyldig karakter.");
			System.exit(2);
		}

		intGrade = strGrade - 'A' + 1;


		System.out.println("Numerisk verdi for " + strGrade + " er " + intGrade);
	}
}
