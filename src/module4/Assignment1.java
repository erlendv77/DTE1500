package module4;

import java.util.Locale;
import java.util.Scanner;

/**
 * Created by erlendvalle on 12.09.2017.
 */
public class Assignment1 {
	public static void main(String[] args) {
		final double RADIUS = 6371.01;

		double x1 = 0;
		double y1 = 0;
		double x2 = 0;
		double y2 = 0;

		Scanner scanner = new Scanner(System.in);

		System.out.print("Skriv inn breddegrad og lengdegrad for punkt 1: ");

//		x1 = Double.valueOf(scanner.next().replace('.', ','));

		x1 = Math.toRadians(scanner.nextDouble());
		y1 = Math.toRadians(scanner.nextDouble());

		System.out.print("Skriv inn breddegrad og lengdegrad for punkt 2: ");
		x2 = Math.toRadians(scanner.nextDouble());
		y2 = Math.toRadians(scanner.nextDouble());

		double result = RADIUS *
				Math.acos((Math.sin(x1) * Math.sin(x2)) + (Math.cos(x1) * Math.cos(x2) * Math.cos(y1 - y2)));

		System.out.println("Avstanden mellom punktene er " + result + " km");
	}
}
