package module4;

import java.util.Scanner;

/**
 * Created by erlendvalle on 13.09.2017.
 */
public class Assignment2 {

	public static void main(String[] args) {
		int input = 0;

		Scanner scanner = new Scanner(System.in);
		System.out.print("Skriv inn et desimaltall mellom 0 og 15: ");
		input = scanner.nextInt();

		if (input < 0 || input > 15) {
			System.err.println(input + " er ikke tillatt.");
			System.exit(32);
		}

		System.out.println(input + " i heksadesimal er " + Integer.toHexString(input).toUpperCase());
	}

}
