package lecture8;

/**
 * Created by erlendvalle on 17.10.2017.
 */
public class Stack {
	private int index = 0;
	private int[] stack;

	public Stack(int size) {
		stack = new int[size];
	}

	public boolean push(int number) {
		if (index < stack.length) {
			stack[index++] = number;

			return true;
		}
		return false;
	}

	public int pop() {
		if (index > 0) {
			return stack[--index];
		}
		return -1;
	}

	/**
	 * resizes the stack, if the new stack is smaller data is lost
	 *
	 * @param newSize
	 */
	public void resize(int newSize) {
		int[] newStack = new int[newSize];
		System.arraycopy(stack, 0, newStack, 0, Math.min(stack.length, newSize));
		index = Math.min(index, newSize);
		stack = newStack;
	}

	public int peek() {
		return stack[index - 1];
	}
}