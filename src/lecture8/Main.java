package lecture8;


import lecture5.BubbleSort;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Created by erlendvalle on 17.10.2017.
 */
public class Main {

	public static void main(String[] args) {
		Stack stack = new Stack(10);
		for(int number = 0; stack.push(number); number++) {
			System.out.println("Pushing " + number);
		}

		System.out.println();

		System.out.println("Popping " + stack.pop());
		System.out.println("Popping " + stack.pop());
		stack.resize(3);
		System.out.println("Popping " + stack.pop());
		System.out.println("Popping " + stack.pop());
	}
}
