package lecture8;

/**
 * Created by erlendvalle on 18.10.2017.
 */
public class Car {
	private boolean running;
	private int speed;
	private int maxSpeed;
	private int horsePower;

	public Car(int maxSpeed, int horsePower) {
		this.maxSpeed = maxSpeed;
		this.horsePower = horsePower;
	}

	public void start() {
		running = true;
	}

	public void stop() {
		running = false;
	}

	public boolean isRunning() {
		return running;
	}

	public int speedUp() {
		if (!running) {
			throw new RuntimeException("Car is not running");
		}
		if (speed == maxSpeed) {
			throw new RuntimeException("This car does not run that fast");
		}
		return ++speed;
	}

	public int slowDown() {
		if (speed > 0) {
			speed--;
		}
		return speed;
	}

	public int getSpeed() {
		return speed;
	}


	public static void main(String[] args) {
		Car car = new Car(250, 100);

		car.start();
		while (car.speedUp() <= 300) {
			System.out.println(car.getSpeed());
		}

		System.out.println();

		while (car.slowDown() > 0) {
			System.out.println(car.getSpeed());
		}

		car.stop();
	}
}
