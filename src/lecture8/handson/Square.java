package lecture8.handson;

public class Square {

	public int width;
	public int height;

	public Square(){
		this.width = 0;
		this.height = 0;
	}


	public Square(int width, int height) {
		this.width = width;
		this.height = height;
		System.out.println("I was created");
	}


	public int getAreal() {
		return this.width * this.height;
	}


	public static void main(String[] args) {
		System.out.println("About to create square");
		Square square = new Square(100, 200);
		System.out.println("About to create square2");
		Square square2 = new Square();
		square2.width = 10;
		square2.height = 20;

		System.out.println(square.getAreal());
		System.out.println(square2.getAreal());


	}


}
