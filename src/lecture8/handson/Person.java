package lecture8.handson;

public class Person {
	public String name;
	public int age;

	public Person(int age) {
		this(null, age);
	}

	public Person(String name, int age) {
		this.name = name;
		this.age = age;

		System.out.printf("%s was created\n", this.name);
	}

	public Person(String name) {
		this(name, 0);
	}



	public static void main(String[] args) {
		Person per = new Person("Per", 20);
		Person paal = new Person("Pål");
		Person askeladded = new Person(30);
	}

}
