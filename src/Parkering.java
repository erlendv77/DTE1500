public class Parkering {
	public static void main(String[] args) {
		System.out.println(beregnPris(26));
	}


	private static double beregnPris(int timer){
		double pris = 0;
		int i = 0;
		while (i < timer){
			i++;
			if (i%24==1){
				pris+=40;
				i+=2;
				continue;
			}

			if (pris%180==0){
				continue;
			}
			pris+=10;
		}
		return pris;
	}
}