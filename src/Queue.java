/**
 * Created by erlendvalle on 09.11.2017.
 */
public class Queue {
// kode for å ta ut ett og ett element av køen, skrive til skjerm ** 1 **

	private int[] elements; // dette er køen
	private int size;

	/**
	 * Construct a queue with the default capacity 8
	 */
	public Queue() {
		elements = new int[8];
	}

	/**
	 * Add a new integer into the queue
	 */
	public void enqueue(int value) {
		if (size >= elements.length) { // Er det plass i arrayet?
			int[] tmpArray = new int[elements.length * 2];
			tmpArray[0] = elements[0];
			tmpArray[elements.length - 1] = elements[elements.length - 1];
			System.arraycopy(elements, 0, tmpArray, 0, 1);
			System.arraycopy(elements, elements.length - 1, tmpArray, tmpArray.length -1, 1);
			elements = tmpArray;
		}
		elements[size++] = value;
	}

	/**
	 * Remove and return an element from the queue
	 */


	/**
	 * Test whether the queue is empty
	 */
	public boolean empty() {
		return size == 0;
	}

	/**
	 * Return the number of elements in the queue
	 */
	public int getSize() {
		return size;
	}

	public int dequeue() {
		int v = elements[0];

		for(int i = 1; i < size; i++) {
			elements[i - 1] = elements[i];
		}

		// siden vi tar ut det første elementet i køen, må vi flytte alle
		// elementene i køen et hakk til venstre, slik at elements[i] flyttes
		// til elements[i – 1] ** 3 **

		size--;
		return v;
	}

	public static void main(String[] args) {
		Queue queue = new Queue();


		queue.enqueue(100);
		queue.enqueue(200);
		int nextValueInQueue = queue.dequeue();
		System.out.println(nextValueInQueue);

	}
}