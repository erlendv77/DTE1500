package module3;

import java.util.Scanner;

/**
 * Created by erlendvalle on 05.09.2017.
 */
public class Assignment1 {
	public static void main(String[] args) {
		final int SCISSORS = 0;
		final int STONE    = 1;
		final int PAPER     = 2;

		int userChoice = 0;
		int computerChoice = (int)(Math.random() * 100) % 3;

		Scanner scanner = new Scanner(System.in);
		System.out.print("Saks (0), stein (1) eller papir (2)?: ");
		userChoice = scanner.nextInt();

		if (userChoice < 0 || userChoice > 2) {
			System.out.println("Ugyldig tall, kun 0, 1 eller 2 aksepteres.");
		} else {
			boolean userWon = false;
			boolean draw = false;

			String strComputerChoice = null;
			String strUserChoice = null;

			switch (userChoice) {
				case STONE:
					strUserChoice = "stein";
					switch (computerChoice) {
						case STONE:
							draw = true;
							break;
						case PAPER:
							userWon = false;
							break;
						case SCISSORS:
							userWon = true;
					}
					break;


				case PAPER:
					strUserChoice = "papir";
					switch (computerChoice) {
						case STONE:
							userWon = true;
							break;
						case PAPER:
							draw = true;
							break;
						case SCISSORS:
							userWon = false;
							break;
					}
					break;

				case SCISSORS:
					strUserChoice = "saks";
					switch (computerChoice) {
						case STONE:
							userWon = false;
							break;
						case PAPER:
							userWon = true;
							break;
						case SCISSORS:
							draw = true;
							break;
					}
			}

			switch (computerChoice) {
				case STONE:
					strComputerChoice = "stein";
					break;
				case PAPER:
					strComputerChoice = "saks";
					break;
				case SCISSORS:
					strComputerChoice = "papir";
					break;
			}

			System.out.print("Datamaskin er " + strComputerChoice + ". Du er " + strUserChoice + " ");
			if (draw) {
				System.out.println("Uavgjort");
			} else if (userWon) {
				System.out.println("Du vant!");
			} else {
				System.out.println("Du tapte :|");
			}
		}
	}
}
