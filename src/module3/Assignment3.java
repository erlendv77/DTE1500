package module3;

import java.util.Scanner;

/**
 * Created by erlendvalle on 05.09.2017.
 */
public class Assignment3 {
	public static void main(String[] args) {
		int fractionTop = 0;
		int fractionBottom = 0;

		Scanner scanner = new Scanner(System.in);
		System.out.print("Oppgi teller: ");
		fractionTop = scanner.nextInt();

		System.out.print("Oppgi nevner: ");
		fractionBottom = scanner.nextInt();


		// 7/3
		// 2

		int decimalResult = fractionTop / fractionBottom;
		if (decimalResult >= 1) {
			System.out.println(fractionTop + " / " + fractionBottom + " er en uekte brøk.");
			System.out.println("Det er et blandet tall " + decimalResult + " + " + (fractionTop - (fractionBottom * decimalResult)) + " / " + fractionBottom);
		} else {
			System.out.println(fractionTop + " / " + fractionBottom + " er en ekte brøk.");
		}


	}
}
