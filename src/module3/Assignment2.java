package module3;

import java.util.Scanner;

/**
 * Created by erlendvalle on 05.09.2017.
 */
public class Assignment2 {
	public static void main(String[] args) {
		double x = 0;
		double y = 0;

		Scanner scanner = new Scanner(System.in);

		System.out.print("Oppgi x og y koordinater for et punkt: ");
		x = scanner.nextDouble();
		y = scanner.nextDouble();

		double distance = Math.sqrt(
				Math.pow(x, 2) + Math.pow(y, 2)
		);

		if (distance <= 10.0) {
			System.out.println("Punktet (" + x + "," + y + ") er inn i sirkelen.");
		} else {
			System.out.println("Punktet (" + x + "," + y + ") er ikke inn i sirkelen.");
		}



	}
}
