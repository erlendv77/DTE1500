package module3;

import java.util.Scanner;

/**
 * Created by erlendvalle on 05.09.2017.
 */
public class Assignment4 {
	public static void main(String[] args) {
		int userInput = 0;

		Scanner scanner = new Scanner(System.in);
		System.out.print("Skriv inn et heltall: ");

		userInput = scanner.nextInt();

		System.out.print("Er " + userInput + " delelig med 5 og 6: ");
		if (userInput % 5 == 0 && userInput % 6 == 0) {
			System.out.println("Ja");
		} else {
			System.out.println("Nei");
		}

		System.out.print("Er " + userInput + " delelig med 5 eller 6: ");
		if (userInput % 5 == 0 || userInput % 6 == 0) {
			System.out.println("Ja");
		} else {
			System.out.println("Nei");
		}

		System.out.print("Er " + userInput + " delelig med 5 eller 6, men ikke begge: ");
		if (userInput % 5 == 0 ^ userInput % 6 == 0) {
			System.out.println("Ja");
		} else {
			System.out.println("Nei");
		}
	}
}
