import javafx.collections.transformation.SortedList;
import testing.Stack;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by erlendvalle on 06/12/2018.
 */
public class Main {


	public static void main(String[] args) {



		ArrayList<Person> list = new ArrayList<Person>(1000);


		list.sort(new Comparator<Person>() {
			@Override
			public int compare(Person o1, Person o2) {
				return o1.compareTo(o2);
			}
		});

		list.addAll(new ArrayList<>());



		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}

	}

}
