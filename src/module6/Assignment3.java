package module6;

/**
 * Created by erlendvalle on 25.09.2017.
 */
public class Assignment3 {


	public static void main(String[] args) {
		for(int year = 2000; year <= 2020; year++) {
			System.out.println("Året " + year + " har " + (isLeapYear(year) ? 366 : 365) + " dager");
		}
	}

	private static boolean isLeapYear(int year) {
		return (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0));
	}
}
