package module6;

import java.util.Scanner;

/**
 * Created by erlendvalle on 25.09.2017.
 */
public class Assignment4 {


	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Skriv inn ditt hemmelige passord: ");
		String password = scanner.next();

		if (isValidPassword(password)) {
			System.out.println("Passordet ditt er gyldig");
		} else {
			System.out.println("Passordet dit er IKKE gyldig");
		}
	}

	private static boolean isValidPassword(String password) {
		return isValidLength(password) && hasCharactersAndAtleastTwoDigits(password);
	}

	private static boolean isValidLength(String password) {
		return password.length() >= 8;
	}

	private static boolean hasCharactersAndAtleastTwoDigits(String password) {
		int numberCount = 0;

		for(int i = 0; i < password.length(); i++) {
			if (isNumber(password.charAt(i))) {
				numberCount++;
			}
			if (!isValidCharacter(password.charAt(i))) {
				return false;
			}
		}

		if (numberCount == password.length()) {
			// only numbers
			return false;
		}

		return numberCount >= 2;
	}

	private static boolean isValidCharacter(char c) {
		return Character.isLetter(c) ||Character.isDigit(c);
	}


	private static boolean isNumber(char c) {
		switch (c) {
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				return true;
			default:
				return false;
		}
	}

}
