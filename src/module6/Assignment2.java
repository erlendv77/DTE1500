package module6;

/**
 * Created by erlendvalle on 25.09.2017.
 */
public class Assignment2 {


	public static void main(String[] args) {

		System.out.println("Celcius         Fahrenheit     | Fahrenheit      Celcius");
		System.out.println("--------------------------------------------------------");
		for(int celcius = 40; celcius > 30; celcius--) {
			int fahrenheit = 120 - ((40 - celcius) * 10);

			System.out.printf("%-15d %-15.2f| %-15d %-15.2f\n",
					celcius,
					celciusToFahrenheit(celcius),
					fahrenheit,
					fahrenheitToCelcius(fahrenheit));

		}
	}


	private static double celciusToFahrenheit(double celcius) {
		return ((9.0 / 5.0) * celcius) + 32.0;
	}

	private static double fahrenheitToCelcius(double fahrenhet) {
		return (5.0 / 9) * (fahrenhet - 32.0);
	}

}
