package module6;

import java.util.Scanner;

/**
 * Created by erlendvalle on 25.09.2017.
 */
public class Assignment1 {


	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Skriv inn et positivt tall: ");
		int input = scanner.nextInt();
		if (isPalindrome(input)) {
			System.out.println(input + " er et palindromtall");
		} else {
			System.out.println(input + " er IKKE et palindromtall");
		}
	}


	private static boolean isPalindrome(int number) {
		if (number == reverse(number)) {
			return true;
		}
		return false;
	}

	private static int reverse(int number) {
		int reversed = 0;

		while(number > 0) {
			reversed = (reversed * 10) + (number % 10);
			number = number / 10;
		}

		return reversed;
	}
}
