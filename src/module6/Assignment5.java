package module6;

import java.util.Scanner;

/**
 * Created by erlendvalle on 25.09.2017.
 */
public class Assignment5 {


	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.printf("Skriv inn et positivt tall: ");

		double input = scanner.nextDouble();
		System.out.println("Tilnærmet verdi for kvadratroten av " + input + " er " + guessSquareRoot(input));

	}

	private static double guessSquareRoot(double number) {
		double guess = number / 2.0;
		double lastGuess;


		boolean foundIt = false;
		while (!foundIt) {
			lastGuess = guess;
			guess = (lastGuess + number / lastGuess) / 2.0;
			foundIt =  lastGuess - guess <= 0.00001;
		}

		return guess;
	}

}
