package testing.ioc;

import java.sql.SQLException;

public class LoginController {

	private UserStorageInterface storage;

	public LoginController(UserStorageInterface storage) {
		this.storage = storage;
	}

	public boolean login(String username, String password) {
		try {
			UserModel user = this.storage.getUserByUsername(username);
			if (user == null) {
				return false;
			}

			return PasswordEncrypter.encrypt(password).equals(user.getPassword());

		} catch (SQLException e) {
			return false;
		}
	}
}
