package testing.ioc;

public class UserAllreadyExistsException extends Exception {
	public UserAllreadyExistsException() {
	}

	public UserAllreadyExistsException(String message) {
		super(message);
	}
}
