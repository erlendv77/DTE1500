package testing.ioc;

import java.sql.SQLException;

public interface UserStorageInterface {
	UserModel getUserById(int id) throws SQLException;

	UserModel getUserByUsername(String username) throws SQLException;

	boolean userExists(String username) throws SQLException;

	void createUser(UserModel userModel) throws SQLException, UserAllreadyExistsException;

	void updateUser(UserModel userModel) throws SQLException, UserDoesntExistException;

	void deleteUser(UserModel userModel) throws SQLException, UserDoesntExistException;
}
