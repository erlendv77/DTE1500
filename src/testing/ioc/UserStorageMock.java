package testing.ioc;

import java.sql.SQLException;

public class UserStorageMock implements UserStorageInterface {

	private UserModel userModel;

	public UserStorageMock(int id, String username, String name, String password) {
		this.userModel = new UserModel(id, username, name, password);
	}

	@Override
	public UserModel getUserById(int id) throws SQLException {
		if (this.userModel != null && id == this.userModel.getId()) {
			return this.userModel;
		}
		return null;
	}

	@Override
	public UserModel getUserByUsername(String username) throws SQLException {
		if (this.userModel != null && username == this.userModel.getUsername()) {
			return this.userModel;
		}
		return null;
	}

	@Override
	public boolean userExists(String username) throws SQLException {
		return this.userModel != null && this.userModel.getUsername().equals(username);
	}

	@Override
	public void createUser(UserModel userModel) throws SQLException, UserAllreadyExistsException {
		this.userModel = userModel;
	}

	@Override
	public void updateUser(UserModel userModel) throws SQLException, UserDoesntExistException {
		this.userModel = userModel;
	}

	@Override
	public void deleteUser(UserModel userModel) throws SQLException, UserDoesntExistException {
		this.userModel = null;
	}
}
