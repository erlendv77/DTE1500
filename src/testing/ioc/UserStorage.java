package testing.ioc;

import java.sql.*;

public class UserStorage implements UserStorageInterface {

	private Connection connection;

	public UserStorage() throws Exception {
		String host = System.getenv("DB_HOST");
		String port = System.getenv("DB_PORT");
		String schema = System.getenv("DB_DATABABSE");
		String user = System.getenv("DB_USER");
		String password = System.getenv("DB_PASSWORD");

		Class.forName("com.mysql.jdbc.Driver");
		this.connection = DriverManager.getConnection( String.format("jdbc:mysql://%s:%s/%s", host, port, schema), user, password );
	}

	private PreparedStatement createQuery(String sql) throws SQLException {
		return this.connection.prepareStatement(sql);

	}

	@Override
	public UserModel getUserById(int id) throws SQLException {
		PreparedStatement statement = this.createQuery("SELECT * FROM user WHERE id = ?");
		statement.setInt(1, id);

		ResultSet resultSet = statement.executeQuery();
		if (!resultSet.next()) {
			return null;
		}

		return new UserModel(
				resultSet.getInt("id"),
				resultSet.getString("username"),
				resultSet.getString("name"),
				resultSet.getString("password")
		);
	}

	@Override
	public UserModel getUserByUsername(String username) throws SQLException {
		PreparedStatement statement = this.createQuery("SELECT * FROM user WHERE username = ?");
		statement.setString(1, username);

		ResultSet resultSet = statement.executeQuery();
		if (!resultSet.next()) {
			return null;
		}

		return new UserModel(
				resultSet.getInt("id"),
				resultSet.getString("username"),
				resultSet.getString("name"),
				resultSet.getString("password")
		);
	}

	@Override
	public boolean userExists(String username) throws SQLException {
		PreparedStatement query = this.createQuery("SELECT id FROM user WHERE username = ?");
		query.setString(1, username);
		return query.executeQuery().next();
	}

	@Override
	public void createUser(UserModel userModel) throws SQLException, UserAllreadyExistsException {
		if (this.userExists(userModel.getUsername())) {
			throw new UserAllreadyExistsException(userModel.getUsername() + " allready exists");
		}
		PreparedStatement statement = this.createQuery("INSERT INTO user (id, username, name, password) VALUES (?, ?, ?, ?)");
		statement.setInt(1, userModel.getId());
		statement.setString(2, userModel.getUsername());
		statement.setString(3, userModel.getName());
		statement.setString(4, userModel.getPassword());
	}

	@Override
	public void updateUser(UserModel userModel) throws SQLException, UserDoesntExistException {
		if (!this.userExists(userModel.getUsername())) {
			throw new UserDoesntExistException(userModel.getUsername() + " doesnt exist");
		}
		PreparedStatement statement = this.createQuery("UPDATE user SET username = ?, name = ?, password = ? WHERE id = ?");
		statement.setString(1, userModel.getUsername());
		statement.setString(2, userModel.getName());
		statement.setString(3, userModel.getPassword());
		statement.setInt(4, userModel.getId());
	}

	@Override
	public void deleteUser(UserModel userModel) throws SQLException, UserDoesntExistException {
		if (!this.userExists(userModel.getUsername())) {
			throw new UserDoesntExistException(userModel.getUsername() + " doesnt exist");
		}
		PreparedStatement statement = this.createQuery("DELETE FROM user WHERE id = ?");
		statement.setInt(1, userModel.getId());
	}

}
