package testing;

import java.util.LinkedList;

/**
 * Created by erlendvalle on 17.10.2017.
 */
public class ListStack {
	private int size;
	private LinkedList<Integer> stack;

	public ListStack(int size) {
		stack = new LinkedList<>();
		this.size = size;
	}

	public boolean push(int number) {
		if (stack.size() < size) {
			stack.addLast(number);

			return true;
		}
		return false;
	}

	public int pop() {
		if (stack.size() > 0) {
			return stack.removeLast();
		}
		return -1;
	}

	/**
	 * resizes the stack, if the new stack is smaller data is lost
	 *
	 * @param newSize
	 */
	public void resize(int newSize) {
		if (newSize > size) {
			size = newSize;
		} else {
			for (int i = size; i > newSize; i--) {
				stack.removeLast();
			}
			size = newSize;
		}
	}

	public int peek() {
		return stack.getLast();
	}

	public int size() {
		return size;
	}

	public int elements() {
		return stack.size();
	}

}