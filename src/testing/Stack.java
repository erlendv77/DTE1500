package testing;

/**
 * Created by erlendvalle on 17.10.2017.
 */
public class Stack {
	private int index = 0;
	private int[] stack;

	public Stack() {
		this(0);
	}

	public Stack(int size) {
		stack = new int[size];
	}

	public boolean push(int number) {
		if (index < stack.length) {
			stack[index++] = number;

			return true;
		}
		return false;
	}

	public int pop() {
		if (index > 0) {
			return stack[--index];
		}
		return -1;
	}

	/**
	 * resizes the stack, if the new stack is smaller data is lost
	 *
	 * @param newSize
	 */
	public void resize(int newSize) {
		int[] newStack = new int[newSize];
		System.arraycopy(stack, 0, newStack, 0, Math.min(stack.length, newSize));
		index = Math.min(index, newSize);
		stack = newStack;
	}

	public int peek() {
		return stack[index - 1];
	}

	public int size() {
		return stack.length;
	}

	public int elements() {
		return index;
	}

	public static void main(String[] args) {
		Stack stack = new Stack(5);
		if (stack.size() != 5) {
			System.out.println("incorrect size of stack (1)");
		}

		if (stack.elements() != 0) {
			System.out.println("incorrect number of elements (1)");
		}
		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.push(4);
		stack.push(5);

		if (stack.elements() != 5) {
			System.out.println("incorrect number of elements (2)");
		}

		if (stack.push(6)) {
			System.out.println("whoops, able to push to many elements on the stack");
		}

		if(stack.peek() != 5) {
			System.out.println("peek didnt return top element");
		}

		if (stack.pop() != 5) {
			System.out.println("pop didnt pop top elements");
		}

		if (stack.elements() != 4) {
			System.out.println("incorrect number of elements after pop");
		}

		stack.resize(2);
		if (stack.size() != 2) {
			System.out.println("incorrect size of stack after resize");
		}

		if (stack.elements() != 2) {
			System.out.println("incorrect number of elements after resize");
		}

		if (stack.peek() != 2) {
			System.out.println("peek did not return top element after resize");
		}

		if (stack.pop() != 2) {
			System.out.println("pop did not pop top element after resize");
		}

		stack.resize(10);

		if (stack.size() != 10) {
			System.out.println("incorrect size of stack after resize (2)");
		}

		if (stack.elements() != 1) {
			System.out.println("incorrect number of elements after resize (2)");
		}

		if (stack.peek() != 1) {
			System.out.println("peek did not return top element after resize (2)");
		}

		if (stack.pop() != 1) {
			System.out.println("pop did not pop top element after resize (2)");
		}
	}
}