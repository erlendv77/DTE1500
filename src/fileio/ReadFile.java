package fileio;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ReadFile {
	public static void main(String[] args) {
		try {
			Scanner scanner = new Scanner(new File("/tmp/data.txt"));

			String text = scanner.nextLine();

			System.out.println(text);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
