package fileio;

import java.io.FileNotFoundException;
import java.io.PrintStream;

public class WriteFile {
	public static void main(String[] args) {


		try {

			PrintStream printStream = new PrintStream("/tmp/data.txt");


			printStream.printf("%s %04d\n", "Hello", 5);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}


	}
}
