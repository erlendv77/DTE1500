package fileio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

public class UrlRead {

	public static void main(String[] args) {
		try {
			URL url = new URL("https://www.vg.no");


			InputStream inputStream = url.openStream();

			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

			for (String line = reader.readLine(); line != null; line = reader.readLine()) {
				System.out.println(line);
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
