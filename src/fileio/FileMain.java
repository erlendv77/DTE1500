package fileio;

import java.io.File;
import java.io.IOException;
import java.util.Date;

public class FileMain {
	public static void main(String[] args) {
		File file = new File("/tmp/newdirectory/");
		if (file.exists()) {
			System.out.println("File exists");
			if (file.isDirectory()) {
				System.out.println("Is directory");
				File[] files = file.listFiles();
				for (int i = 0; i < files.length; i++) {
					File childFile = files[i];
					System.out.println(childFile.getName());
				}
			}
			if (file.isFile()) {
				System.out.println("Is file");
				System.out.println("File is " + file.length() + "bytes");
			}

			System.out.println("File was last modified " + new Date(file.lastModified()).toString());
			System.out.println("Filename: " + file.getName());

			if (file.isAbsolute()) {
				System.out.println("Absolut file path");
			} else {
				System.out.println("Absolut path: " + file.getAbsolutePath());
			}


		} else {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}



	}
}
