import java.util.Scanner;

/**
 * Created by erlendvalle on 05.12.2017.
 */
public class Shop {

	public static void main(String[] args) {
		int[][] salgsTabell = new int[5][4];
		int[] prisTabell = {200, 400, 800, 5000};

		int selgerNummer = 0;
		int produktNummer = 0;
		int antall = 0;

		Scanner scanner = new Scanner(System.in);
		boolean processingInput = true;
		while (processingInput) {
			System.out.print("Angi selgernr (-1 avslutter): ");
			selgerNummer = scanner.nextInt();
			if (selgerNummer != -1) {
				if (selgerNummer > 0 && selgerNummer <= 5) {
					System.out.print("Oppgi produktnummer: ");
					produktNummer = scanner.nextInt();
					if (produktNummer > 0 && produktNummer <= 4) {
						System.out.print("Angi antall: ");
						antall = scanner.nextInt();
						salgsTabell[selgerNummer - 1][produktNummer - 1] += antall;
					}
				}
			} else {
				processingInput = false;
			}
		}

		for(int i = 0; i < salgsTabell.length; i++) {
			int sum = 0;
			for(int a = 0; a < salgsTabell[i].length; a++) {
				sum += salgsTabell[i][a] * prisTabell[a];
			}
			System.out.printf("Selger : %d kr %d\n", (i + 1), sum);
		}
	}
}
