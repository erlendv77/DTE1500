package module5;

/**
 * Created by erlendvalle on 18.09.2017.
 */
public class Assignment3 {
	public static void main(String[] args) {
		double pi;

		double sumTerms = 0;
		for(int i = 1; i <= 100000; i++) {
//			                  1   /           (i * 2)   - 1
			double fraction = 1.0 / (( (double)i * 2.0) - 1.0 );
			if (i % 2 == 1) {
				sumTerms += fraction;
			} else {
				sumTerms -= fraction;
			}
			if (i % 10000 == 0) {
				pi = 4.0 * sumTerms;
				System.out.println("For " + i + " ledd i rekken er verdien av pi: " + pi);
			}

		}
	}
}
