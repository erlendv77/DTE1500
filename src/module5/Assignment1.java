package module5;

import java.util.Scanner;

/**
 * Created by erlendvalle on 18.09.2017.
 */
public class Assignment1 {
	public static void main(String[] args) {
		String input;
		String result = "";

		Scanner scanner = new Scanner(System.in);
		System.out.print("Skriv inn tekststrengen: ");
		input = scanner.nextLine();

		for(int i = input.length() - 1; i >= 0; i--) {
			result += input.charAt(i);
		}

		System.out.println("Reversen av " + input + " er " + result);
	}
}
