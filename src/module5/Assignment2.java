package module5;

import java.util.Scanner;

/**
 * Created by erlendvalle on 18.09.2017.
 */
public class Assignment2 {
	public static void main(String[] args) {
		int numberOfStudents;
		int bestScore = -1;
		String bestStudent = "";
		int nestBestScore = -1;
		String nestBestStudent = "";

		Scanner scanner = new Scanner(System.in);
		System.out.print("Oppgi antall studenter: ");
		numberOfStudents = scanner.nextInt();
		scanner.nextLine();

		for(int i = 0; i < numberOfStudents; i++) {
			System.out.print("Oppgi studentens score og navn: ");
			String input = scanner.nextLine();
			int index = input.indexOf(' ');
			int score = Integer.valueOf(input.substring(0, index));
			String studentName = input.substring(index + 1);

			if (score > bestScore) {
				nestBestScore = bestScore;
				nestBestStudent = bestStudent;

				bestScore = score;
				bestStudent = studentName;
			} else if (score > nestBestScore) {
				nestBestScore = score;
				nestBestStudent = studentName;
			}
		}

		if (numberOfStudents >= 2) {
			System.out.println("Topp to studenter:");
			System.out.println("Første plass: " + bestStudent + " med score: " + bestScore);
			System.out.println("Andre plass: " + nestBestStudent + " med score: " + nestBestScore);
		} else if (numberOfStudents >= 1) {
			System.out.println("Beste student:");
			System.out.println(bestStudent + " med score: " + bestScore);

		}
	}
}
