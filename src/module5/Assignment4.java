package module5;

/**
 * Created by erlendvalle on 19.09.2017.
 */
public class Assignment4 {
	public static void main(String[] args) {
		final int layers = 20;

		for (int a = 0; a < layers; a++) {


			for(int b = 0; b < layers; b++) {
				if (layers - 1 - a - b > 0) {
					System.out.print("     ");
				} else {
					System.out.printf("%5d", (int)Math.pow(2, (a - layers + b + 1)));
				}
			}

			for(int b = layers - 1; b >= 0; b--) {
				if (layers - a - b > 0) {
					System.out.print("     ");
				} else {
					System.out.printf("%5d", (int)Math.pow(2, a - layers + b));
				}
			}


			System.out.println();
		}
	}
}
