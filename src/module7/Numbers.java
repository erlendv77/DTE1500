package module7;

import java.util.Scanner;

/**
 * Created by erlendvalle on 11.10.2017.
 */
public class Numbers {
	public static void main(String[] args) {
		int[] numbers = new int[100];

		Scanner scanner = new Scanner(System.in);
		System.out.println("Skriv inn tall mellom 1 og 100, 0 avslutter programmet: ");


		for(int input = Integer.valueOf(scanner.nextLine()); input >= 1 && input <= 100; input = Integer.valueOf(scanner.nextLine())) {
			numbers[input - 1]++;
		}

		for(int i = 0; i < numbers.length; i++) {
			if (numbers[i] > 0) {
				System.out.printf("%3d ble oppgitt %d ganger\n", i + 1, numbers[i]);
			}
		}
	}
}
