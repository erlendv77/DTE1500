package module7;

import java.util.Scanner;

/**
 * Created by erlendvalle on 11.10.2017.
 */
public class BestStudents {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.print("Oppgi antall studenter: ");
		int numberOfStudents = scanner.nextInt();

		String[] studentNames = new String[numberOfStudents];
		int[] scores = new int[numberOfStudents];

		scanner.nextLine();
		for(int i = 0; i < numberOfStudents; i++) {
			System.out.print("Navn:     ");
			studentNames[i] = scanner.nextLine();
			System.out.print("Resultat: ");
			scores[i] = Integer.valueOf(scanner.nextLine());
		}


		for(int a = 0; a < numberOfStudents - 1; a++) {
			int min = a ;
			for(int b = a + 1; b < numberOfStudents; b++) {
				if (scores[b] > scores[min]) {
					swap(scores, b, min);
					swap(studentNames, b, min);
				}
			}
		}


		System.out.printf("%-20sResultat\n", "Navn");
		for(int i = 0; i < numberOfStudents; i++) {
			System.out.printf("%-20s%3.2f\n", studentNames[i], (double)scores[i]);
		}

	}

	public static void swap(int[] array, int a, int b) {
		int tmp = array[a];
		array[a] = array[b];
		array[b] = tmp;
	}

	public static void swap(String[] array, int a, int b) {
		String tmp = array[a];
		array[a] = array[b];
		array[b] = tmp;
	}
}
