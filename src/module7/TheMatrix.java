package module7;

import java.util.Scanner;

/**
 * Created by erlendvalle on 11.10.2017.
 */
public class TheMatrix {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		double[][] numbers = new double[4][4];

		System.out.printf("Oppgi en %dx%d matrise rad for rad:\n", numbers.length, numbers[0].length);
		for(int x = 0; x < numbers.length; x++) {
			for(int y = 0; y < numbers[x].length; y++) {
				numbers[x][y] = scanner.nextDouble();
			}
		}

		System.out.println();
		for(int x = 0; x < numbers.length; x++) {
			for(int y = 0; y < numbers[x].length; y++) {
				System.out.printf("%3d", (int)numbers[x][y]);
			}
			System.out.println();
		}
		System.out.println();


		double sum = 0;
		for(int x = 0; x < numbers.length; x++) {
			for(int y = 0; y < numbers[x].length; y++) {
				if (x == y) {
					sum += numbers[x][y];
				}
			}
		}


		System.out.printf("%-40s%4.2f\n", "Summen av tall i diagonalen er:", sum);
		System.out.printf("%-40s%4.2f\n", "Gjennomsnittet av tall i diagonalen er:", (sum / (double)numbers.length));

	}
}
