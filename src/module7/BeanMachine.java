package module7;

import java.util.Scanner;

/**
 * Created by erlendvalle on 11.10.2017.
 */
public class BeanMachine {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Oppgi antall baller: ");
		int numberOfBalls = Integer.valueOf(scanner.nextLine());
		System.out.print("Oppgi antall spor: ");
		int numberOfSlots = Integer.valueOf(scanner.nextLine());

		int[] slots = new int[numberOfSlots];

		for(int ball = 0; ball < numberOfBalls; ball++) {
			int path = getPathForOneBall(numberOfSlots);
			slots[path]++;
		}
		System.out.println();
		printResult(slots);
	}


	private static void printResult(int[] slots) {
		int maxBalls = slots[maxIndex(slots)];

		for(int i = maxBalls - 1; i >= 0; i--) {
			for(int a = 0; a < slots.length; a++) {
				if (slots[a] > i) {
					System.out.print(" O ");
				} else {
					System.out.print("   ");
				}
			}
			System.out.println();
		}
		System.out.print("|");
		for(int i = 0; i < slots.length; i++) {
			System.out.print(i + " |");
		}
		System.out.println();
	}

	private static int maxIndex(int[] slots) {
		int maxIndex = 0;
		for(int i = 1; i < slots.length; i++) {
			if (slots[i] > slots[maxIndex]) {
				maxIndex = i;
			}
		}
		return maxIndex;
	}


	private static int getPathForOneBall(int numberOfSlots) {
		int slot = 0;
		for(int level = 0; level < numberOfSlots - 1; level++) {
			boolean right = (int)(Math.random() * 100) % 2 == 0;
			if (right) {
				System.out.print("R");
				slot++;
			} else {
				System.out.print("L");
			}
		}
		System.out.println();
		return slot;
	}
}
