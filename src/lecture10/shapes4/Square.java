package lecture10.shapes4;

import java.awt.*;

/**
 * Created by erlendvalle on 31.10.2017.
 */
public class Square extends Shape {
	private int width;
	private int height;

	public Square() {
	}

	public Square(int x, int y) {
		super(x, y);
	}

	public Square(int x, int y, int width, int height, Color color, boolean fill) {
		super(x, y, color, fill);
		this.width = width;
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public void fill(Graphics graphics) {
		graphics.fillRect(x, y, width, height);
	}

	public void draw(Graphics graphics) {
		graphics.drawRect(x, y, width, height);
	}
}
