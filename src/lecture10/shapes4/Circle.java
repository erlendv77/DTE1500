package lecture10.shapes4;

import java.awt.*;

/**
 * Created by erlendvalle on 31.10.2017.
 */
public class Circle extends Shape {
	private int radius;

	public Circle() {
	}

	public Circle(int x, int y) {
		super(x, y);
	}

	public Circle(int x, int y, int radius, Color color, boolean fill) {
		super(x, y, color, fill);
		this.radius = radius;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public void fill(Graphics graphics) {
		graphics.fillOval(x, y, radius * 2, radius * 2);
	}

	public void draw(Graphics graphics) {
		graphics.drawOval(x, y, radius * 2, radius * 2);
	}
}
