package lecture10.shapes4;

import java.awt.*;

/**
 * Created by erlendvalle on 31.10.2017.
 */
public class Polygon extends Shape {
	private int[][] points;

	public Polygon() {
	}

	public Polygon(int x, int y) {
		super(x, y);
	}

	public Polygon(int x, int y, int[][] points, Color color, boolean fill) {
		super(x, y, color, fill);
		this.points = transformPoints(points);
	}

	private int[][] transformPoints(int[][] points) {
		int[][] newPoints = new int[2][points.length + 1];
		for (int i = 0; i < points.length; i++) {
			int[] point = points[i];
			newPoints[0][i] = point[0] + x;
			newPoints[1][i] = point[1] + y;
		}

		newPoints[0][points.length] = points[0][0] + x;
		newPoints[1][points.length] = points[0][1] + y;

		return newPoints;
	}

	public void fill(Graphics graphics) {
		graphics.fillPolygon(this.points[0], this.points[1], this.points[0].length);
	}

	public void draw(Graphics graphics) {
		graphics.drawPolygon(this.points[0], this.points[1], this.points[0].length);
	}
}
