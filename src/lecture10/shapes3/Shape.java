package lecture10.shapes3;

import java.awt.*;

/**
 * Created by erlendvalle on 31.10.2017.
 */
public abstract class Shape {
	protected int x;
	protected int y;
	protected Color color;
	protected boolean fill;

	public Shape() {
	}

	public Shape(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public Shape(int x, int y, Color color, boolean fill) {
		this.x = x;
		this.y = y;
		this.color = color;
		this.fill = fill;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public boolean isFill() {
		return fill;
	}

	public void setFill(boolean fill) {
		this.fill = fill;
	}

	public abstract void paint(Graphics g);


}
