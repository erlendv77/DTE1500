package lecture10.shapes3;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by erlendvalle on 31.10.2017.
 */
public class ShapeDrawer extends JComponent {
	private ArrayList<Shape> shapes = new ArrayList<Shape>();


	public void add(Shape shape) {
		this.shapes.add(shape);
	}

	@Override
	public void paint(Graphics g) {
		((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		for (int i = 0; i < shapes.size(); i++) {
			Shape shape = shapes.get(i);
			shape.paint(g);
		}
	}
}
