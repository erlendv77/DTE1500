package lecture10.shapes3;

import java.awt.*;

/**
 * Created by erlendvalle on 31.10.2017.
 */
public class Circle extends Shape {
	private int radius;

	public Circle() {
	}

	public Circle(int x, int y) {
		super(x, y);
	}

	public Circle(int x, int y, int radius, Color color, boolean fill) {
		super(x, y, color, fill);
		this.radius = radius;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public void paint(Graphics graphics) {
		graphics.setColor(color);
		if (fill) {
			graphics.fillOval(x, y, radius * 2, radius * 2);
		} else {
			graphics.drawOval(x, y, radius * 2, radius * 2);
		}
	}
}
