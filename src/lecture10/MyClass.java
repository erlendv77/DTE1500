package lecture10;

public class MyClass
{
	protected String message;

	public MyClass() {
	}

	public MyClass(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void printMessage() {
		System.out.println(message);
	}
}
