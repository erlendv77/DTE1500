package lecture10.shapes5;

import java.awt.*;

/**
 * Created by erlendvalle on 31.10.2017.
 */
public class Polygon extends DefaultLogable implements Shape {
	private int[][] points;
	private Color color;
	private boolean fill;

	public Polygon() {
	}


	public Polygon(int[][] points, Color color, boolean fill) {
		this.points = transformPoints(points);
		this.color = color;
		this.fill = fill;
	}

	private int[][] transformPoints(int[][] points) {
		int[][] newPoints = new int[2][points.length + 1];
		for (int i = 0; i < points.length; i++) {
			int[] point = points[i];
			newPoints[0][i] = point[0];
			newPoints[1][i] = point[1];
		}

		newPoints[0][points.length] = points[0][0];
		newPoints[1][points.length] = points[0][1];

		return newPoints;
	}

	@Override
	public void paint(Graphics graphics) {
		graphics.setColor(color);
		if (fill) {
			graphics.fillPolygon(this.points[0], this.points[1], this.points[0].length);
		} else {
			graphics.drawPolygon(this.points[0], this.points[1], this.points[0].length);
		}
	}


}
