package lecture10.shapes5;

import javax.swing.*;
import java.awt.*;

/**
 * Created by erlendvalle on 21/11/2018.
 */
public class Main {
	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.getContentPane().setLayout(new BorderLayout());
		ShapeDrawer shapeDrawer = new ShapeDrawer();

		Logger logger = new Logger();

		Circle circle = new Circle(10, 10, 100, Color.blue, true);



		Square square = new Square(10, 10, 100, 100, Color.red, true);
		shapeDrawer.add(square);
		Polygon polygon = new Polygon(
				new int[][]{{0, 0}, {135, 15}, {15, 135}, {135, 135}},
				Color.orange, true);
		shapeDrawer.add(polygon);


		shapeDrawer.add(circle);
		logger.log(circle);



		logger.log(square);
		logger.log(polygon);

		frame.add(shapeDrawer, BorderLayout.CENTER);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(1024, 768);
		frame.setVisible(true);
	}
}
