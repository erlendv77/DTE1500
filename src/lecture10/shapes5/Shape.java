package lecture10.shapes5;

import java.awt.*;

/**
 * Created by erlendvalle on 01.11.2017.
 */
public interface Shape {

	public void paint(Graphics graphics);


}
