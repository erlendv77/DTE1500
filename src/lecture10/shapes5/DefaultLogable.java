package lecture10.shapes5;

public class DefaultLogable implements Logable {
	@Override
	public void debug() {
		System.out.printf("%s is calling debug", getClass().getName());
	}
}
