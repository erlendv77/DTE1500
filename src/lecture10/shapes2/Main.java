package lecture10.shapes2;

import javax.swing.*;
import java.awt.*;

/**
 * Created by erlendvalle on 21/11/2018.
 */
public class Main {
	public static void main(String[] args) {

		JFrame frame = new JFrame();
		ShapeDrawer shapeDrawer = new ShapeDrawer();


		Shape shape = new Square();

		Circle c = new Circle();

		c.x = 10;




		Circle circle = new Circle(10, 10, 100, Color.blue, false);

		shapeDrawer.add(circle);
		Square square = new Square(10, 10, 100, 100, Color.red, true);
		square.setX(20);
		shapeDrawer.add(square);


		frame.add(shapeDrawer);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(1024, 768);
		frame.setVisible(true);
	}

	public static void test(Shape shape) {
		Square square = (Square)shape;
	}
}
