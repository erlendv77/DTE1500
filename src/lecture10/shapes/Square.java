package lecture10.shapes;

import java.awt.*;

/**
 * Created by erlendvalle on 31.10.2017.
 */
public class Square {
	private int x;
	private int y;
	private Color color;
	private boolean fill;
	private int width;
	private int height;

	public Square() {
	}

	public Square(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public Square(int x, int y, int width, int height, Color color, boolean fill) {
		this(x, y);
		this.width = width;
		this.height = height;
		this.color = color;
		this.fill = fill;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public boolean isFill() {
		return fill;
	}

	public void setFill(boolean fill) {
		this.fill = fill;
	}

	public void paint(Graphics graphics) {
		graphics.setColor(color);
		if (fill) {
			graphics.fillRect(x, y, width, height);
		} else {
			graphics.drawRect(x, y, width, height);
		}
	}
}
