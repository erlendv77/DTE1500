package lecture10.shapes;

import java.awt.*;

/**
 * Created by erlendvalle on 31.10.2017.
 */
public class Circle {
	private int x;
	private int y;
	private Color color;
	private boolean fill;
	private int radius;

	public Circle() {
	}

	public Circle(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public Circle(int x, int y, int radius, Color color, boolean fill) {
		this(x, y);
		this.radius = radius;
		this.color = color;
		this.fill = fill;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public boolean isFill() {
		return fill;
	}

	public void setFill(boolean fill) {
		this.fill = fill;
	}

	public void paint(Graphics graphics) {
		graphics.setColor(color);
		if (fill) {
			graphics.fillOval(x, y, radius * 2, radius * 2);
		} else {
			graphics.drawOval(x, y, radius * 2, radius * 2);
		}
	}
}
