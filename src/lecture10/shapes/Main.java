package lecture10.shapes;

import javax.swing.*;
import java.awt.*;

/**
 * Created by erlendvalle on 21/11/2018.
 */
public class Main {
	public static void main(String[] args) {
		JFrame frame = new JFrame();
		ShapeDrawer shapeDrawer = new ShapeDrawer();

		shapeDrawer.add(new Circle(10, 10, 100, Color.blue, false));
		shapeDrawer.add(new Circle(100, 50, 100, Color.red, true));
		shapeDrawer.add(new Square(10, 10, 100, 100, Color.green, true));

		frame.add(shapeDrawer);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(1024, 768);
		frame.setVisible(true);
	}
}
