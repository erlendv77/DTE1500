package lecture10.shapes;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by erlendvalle on 31.10.2017.
 */
public class ShapeDrawer extends JComponent {
	private ArrayList<Square> squares = new ArrayList<Square>();
	private ArrayList<Circle> circles = new ArrayList<Circle>();


	public void add(Square square) {
		this.squares.add(square);
	}

	public void add(Circle circle) {
		this.circles.add(circle);
	}

	@Override
	public void paint(Graphics g) {
		((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		for (int i = 0; i < squares.size(); i++) {
			Square square = squares.get(i);
			square.paint(g);
		}
		for (int i = 0; i < circles.size(); i++) {
			Circle circle = circles.get(i);
			circle.paint(g);
		}
	}
}
