package lecture4;

/**
 * Created by erlendvalle on 20.09.2017.
 */
public class PiCalculation2 {


	public static double calculatePi(int numberOfTerms) {
		double sumTerms = 0;
		for(int i = 1; i <= numberOfTerms; i++) {
			double fraction = 1.0 / (( (double)i * 2.0) - 1.0 );
			if (i % 2 == 1) {
				sumTerms += fraction;
			} else {
				sumTerms -= fraction;
			}
		}
		return 4.0 * sumTerms;
	}

	public static void main(String[] args) {
		System.out.println("For 10 000 ledd i rekken er verdien av pi: " + PiCalculation2.calculatePi(10000));
		System.out.println("For 100 000 ledd i rekken er verdien av pi: " + PiCalculation2.calculatePi(100000));
		System.out.println("For 1 000 000 ledd i rekken er verdien av pi: " + PiCalculation2.calculatePi(1000000));


	}
}
