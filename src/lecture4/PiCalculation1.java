package lecture4;

/**
 * Created by erlendvalle on 20.09.2017.
 */
public class PiCalculation1 {
	public static void main(String[] args) {
		// Calculates pi for 10 000, 100 000 and 1 000 000 terms
		double pi;

		double sumTerms = 0;
		for(int i = 1; i <= 10000; i++) {
			double fraction = 1.0 / (( (double)i * 2.0) - 1.0 );
			if (i % 2 == 1) {
				sumTerms += fraction;
			} else {
				sumTerms -= fraction;
			}
		}
		pi = 4.0 * sumTerms;
		System.out.println("For 10 000 ledd i rekken er verdien av pi: " + pi);




		sumTerms = 0;
		for(int i = 1; i <= 100000; i++) {
			double fraction = 1.0 / (( (double)i * 2.0) - 1.0 );
			if (i % 2 == 1) {
				sumTerms += fraction;
			} else {
				sumTerms -= fraction;
			}
		}
		pi = 4.0 * sumTerms;
		System.out.println("For 100 000 ledd i rekken er verdien av pi: " + pi);




		sumTerms = 0;
		for(int i = 1; i <= 1000000; i++) {
			double fraction = 1.0 / (( (double)i * 2.0) - 1.0 );
			if (i % 2 == 1) {
				sumTerms += fraction;
			} else {
				sumTerms -= fraction;
			}
		}
		pi = 4.0 * sumTerms;
		System.out.println("For 1 000 000 ledd i rekken er verdien av pi: " + pi);
	}
}
