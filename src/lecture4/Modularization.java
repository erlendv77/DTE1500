package lecture4;

import java.util.Scanner;

/**
 * Created by erlendvalle on 20.09.2017.
 */
public class Modularization {


	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);


		int input;


		String s = "hallo";
		for(int i = 0; i < 10; i++) {

			s += i;
			System.out.println(s);
		}

	}


	public static double calculatePi(int numberOfTerms) {
		double sumTerms = 0;
		for(int i = 1; i <= numberOfTerms; i++) {
			double fraction = 1.0 / (( (double)i * 2.0) - 1.0 );
			if (i % 2 == 1) {
				sumTerms += fraction;
			} else {
				sumTerms -= fraction;
			}
		}
		return 4.0 * sumTerms;
	}
	private static int i = 0;
}
