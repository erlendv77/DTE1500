/**
 * Created by erlendvalle on 12/12/2018.
 */
public class Pakke {

	double width;
	double height;
	double depth;
	double weight;
	private static int count = 0;

	public Pakke() {
		this(0.2, 0.2, 0.2, 0.2);
	}

	public Pakke(double width, double height, double depth, double weight) {
		this.width = width;
		this.height = height;
		this.depth = depth;
		this.weight = weight;

		count++;
	}

	private double volum() {
		return height * width * depth;
	}

	public double beregnPris() {
		if (weight <= 1.0) {
			return 50.0;
		}
		if (volum() > 0.5) {
			return 200.0;
		}
		return 100.0;
	}

	public static int antall() {
		return count;
	}


	public static void main(String[] args) {
		Pakke pakke1 = new Pakke(2, 2, 2, 1.1);
		Pakke pakke2 = new Pakke();

		System.out.println(pakke1.beregnPris());
		System.out.println(pakke2.beregnPris());

		System.out.println(Pakke.antall());
	}

}
