

public class Eiendom {
    private String adresse;
    private int antallRom;
    private int antallKvm;
    private double[] bud = new double[20];
    private static int antallEiendommer = 0;
    private int budIndex = -1;

    public Eiendom(String adresse, int antallRom, int antallKvm) {
        this.adresse = adresse;
        this.antallRom = antallRom;
        this.antallKvm = antallKvm;
        antallEiendommer++;
    }

    public void nyttBud(double budBelop) {
       if(budIndex < 0 || budBelop > bud[budIndex]) {
            budIndex++;
            if(budIndex >= bud.length) {
                double[] nyBudTabell = new double[bud.length * 2];
                for(int i = 0; i < bud.length; i++) {
                    nyBudTabell[i] = bud[i];
                }
                bud = nyBudTabell;
            }
            bud[budIndex] = budBelop;
       } else {
            System.out.println("Budet er lavere enn høyeste bud");
       }
    }

    public void skrivEiendom() {
        System.out.println("Adresse: " + adresse);
        System.out.println("Antall rom: " + antallRom);
        System.out.println("Antall kvm: " + antallKvm);
        System.out.println("Følgende bud er gitt:");
        for(int i = budIndex; i >= 0; i--) {
            System.out.println(bud[i]);
        }
    }

    public static int getAntallEiendommer() {
        return antallEiendommer;
    }

    public static void main(String[] args) {
        Eiendom eiendom1 = new Eiendom("Dronningens gate 62", 4, 120);
        Eiendom eiendom2 = new Eiendom("Kongens gate 43", 3, 90);

        eiendom1.nyttBud(5800000);
        eiendom1.nyttBud(5400000);
        eiendom1.nyttBud(5900000);

        eiendom2.nyttBud(4200000);
        eiendom2.nyttBud(4300000);
        eiendom2.nyttBud(4350000);

        eiendom1.skrivEiendom();
        eiendom2.skrivEiendom();

        System.out.println("Antall eiendommer: " + getAntallEiendommer());
    }
}



