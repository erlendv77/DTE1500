package lecture9;

/**
 * Created by erlendvalle on 26.10.2017.
 */
public class Course {
	private String name;
	private String desciption;

	public Course(String name, String desciption) {
		this.name = name;
		this.desciption = desciption;
	}

	public String getName() {
		return name;
	}

	public String getDesciption() {
		return desciption;
	}
}
