package lecture9.shapes;

import java.awt.*;

/**
 * Created by erlendvalle on 26.10.2017.
 */
public interface Shape {

	public void draw();
}
