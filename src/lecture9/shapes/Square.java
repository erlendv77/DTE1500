package lecture9.shapes;

/**
 * Created by erlendvalle on 26.10.2017.
 */
public class Square implements Shape {
	private int width, height;
	private int x, y;

	public Square(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public void draw() {
		System.out.printf("Draws a square (%dx%d) on %dx%d\n", this.width, this.height, this.x, this.y);
	}
}
