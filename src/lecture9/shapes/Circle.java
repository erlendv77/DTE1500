package lecture9.shapes;

import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by erlendvalle on 26.10.2017.
 */
public class Circle implements Shape, Comparable {
	private int radius, x, y;

	public Circle(int x, int y, int radius) {
		this.x = x;
		this.y = y;
		this.radius = radius;
	}

	public void draw() {
		System.out.printf("Draws a circle with radius %d on %dx%d\n", this.radius, this.x, this.y);
	}

	@Override
	public int compareTo(Object o) {
		return 0;
	}
}
