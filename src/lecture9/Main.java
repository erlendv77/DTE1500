package lecture9;

import lecture9.shapes.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

/**
 * Created by erlendvalle on 26.10.2017.
 */
public class Main {


	public static void main(String[] args) {


		HashMap<Integer, String> map = new HashMap<Integer, String>();


		map.put(1, "hei");

	}

	public static void myMethod(Integer integert) {

	}

	public static void printCourses(StudentManagement management, Student student) {
		ArrayList<Course> courses = management.getCourses(student);
		for (int i = 0; i < courses.size(); i++) {
			Course course = courses.get(i);
			System.out.println(student.getName() + " is attending " + course.getName());
		}
	}

	public static void printStudent(StudentManagement management, Course course) {
		System.out.println("-----" + course.getName() + "-----------");
		ArrayList<Student> students = management.getStudents(course);
		for (int i = 0; i < students.size(); i++) {
			Student student = students.get(i);
			System.out.println("--- " + student.getName());
		}
	}

	private static Date getBirth(int year, int month, int day) {
		GregorianCalendar cal = new GregorianCalendar(year, month, day);
		return cal.getTime();
	}

}
