package lecture9;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by erlendvalle on 26.10.2017.
 */
public class Student {
	private String name;
	private Date birth;


	public Student(String name, Date birth) {
		this.name = name;
		this.birth = birth;
	}

	public String getName() {
		return name;
	}

	public Date getBirth() {
		return birth;
	}

	public String toString() {
		return name;
	}

	public boolean equals(Object o) {
		if (!(o instanceof Student)) {
			return false;
		}

		Student s = (Student)o;
		return s.getName().equals(name) && s.getBirth().equals(birth);
	}
}
