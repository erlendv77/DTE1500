package lecture9;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by erlendvalle on 15/11/2018.
 */
public class StudentManagement {
	private HashMap<Course, ArrayList<Student>> courseStudents = new HashMap<Course, ArrayList<Student>>();
	private HashMap<Student, ArrayList<Course>> studentCourses = new HashMap<Student, ArrayList<Course>>();


	public void enroll(Student student, Course course) {
		ArrayList<Student> students = courseStudents.get(course);
		if (students == null) {
			students = new ArrayList<Student>();
			courseStudents.put(course, students);
		}
		students.add(student);


		ArrayList<Course> courses = studentCourses.get(student);
		if (courses == null) {
			courses = new ArrayList<Course>();
			studentCourses.put(student, courses);
		}
		courses.add(course);
	}

	public ArrayList<Student> getStudents(Course course) {
		return courseStudents.get(course);
	}

	public ArrayList<Course> getCourses(Student student) {
		return studentCourses.get(student);
	}


}
