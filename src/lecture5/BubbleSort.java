package lecture5;

import java.util.Arrays;

/**
 * Created by erlendvalle on 26.09.2017.
 */
public class BubbleSort {


	public static void main(String[] args) {
		int[] toSort = {23, 212, 23, 43, 45, 12, 23};

		System.out.println(Arrays.toString(toSort));

		sort(toSort);

		System.out.println(Arrays.toString(toSort));

	}

	public static void sort(int[] array) {
		boolean sorted = false;

		while (!sorted) {
			sorted = true; // Lets start optimistic
			for(int i = 0; i < array.length - 1; i++) {
				if (array[i] > array[i + 1]) {
					sorted = false;
					int tmp = array[i];
					array[i] = array[i + 1];
					array[i + 1] = tmp;
				}
			}
		}
	}
}
