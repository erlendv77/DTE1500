package lecture5;

public class InputParameters {

	public static void main(String[] args) {
		if (args.length != 1) {
			System.err.println("Usage: InputParameters <name>");
			System.exit(1);
		}

		System.out.printf("Hello %s\n", args[0]);
	}

}
