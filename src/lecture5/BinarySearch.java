package lecture5;

public class BinarySearch {

	public static void main(String[] args) {
		int[] data = {1, 2, 3, 4, 5, 6, 7, 8, 9};

		System.out.println(binarySearch(data, 10));
	}


	public static int binarySearch(int[] array, int needle) {
		int left = 0;
		int right = array.length - 1;
		while (left <= right) {
			int index = (left + right) /2;
			if (array[index] < needle) {
				left = index + 1;
			} else if (array[index] > needle) {
				right = index - 1;
			} else {
				return index;
			}
		}
		return -1;
	}
}
