/**
 * Created by erlendvalle on 06.09.2017.
 */
public class Person implements Comparable<Person>{
	private String name;

	public Person(String name) {
		this.name = name;
	}

	public String toString() {
		return name;
	}

	@Override
	public int compareTo(Person o) {
		return name.compareTo(o.name);
	}
}

