package module2;

import java.util.Scanner;

/**
 * Created by erlendvalle on 04.09.2017.
 */
public class Assignment2 {


	public static void main(String[] args) {
		final int SECONDS_IN_A_DAY = 60 * 60 * 24;
		final int SECONDS_IN_AN_HOUR = 60 * 60;

		int totalSeconds = 0;

		Scanner scanner = new Scanner(System.in);

		System.out.print("Oppgi antall sekunder: ");
		totalSeconds = scanner.nextInt();

		int days =  totalSeconds / SECONDS_IN_A_DAY;
		totalSeconds -= days * SECONDS_IN_A_DAY;

		int hours = totalSeconds / SECONDS_IN_AN_HOUR;
		totalSeconds -= hours * SECONDS_IN_AN_HOUR;

		int minutes = totalSeconds / 60;
		totalSeconds -= minutes * 60;

		int seconds = totalSeconds;

		System.out.println(days + " dag(er), " + hours + " time(r), " + minutes + " minutt(er), " + seconds + " sekund(er)");

		String strDays = "dager";
		if (days == 1) {
			strDays = "dag";
		}

		System.out.print(days + " ");
		if (days == 1) {
			System.out.print(" dag ");
		} else {
			System.out.print(" dager ");
		}
		System.out.println();

		System.out.println(
				days + " " + strDays + ", " +
				hours + " time" + (hours != 1 ? "r" : "") +
				hours + (hours == 1 ? " time" : " timer") + ", " +
				minutes + (minutes == 1 ? " minutt" : " minutter") + " og " +
				seconds + (seconds == 1 ? " sekund " : " sekunder ")
		);

	}

}
