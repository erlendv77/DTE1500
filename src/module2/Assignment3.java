package module2;

import java.util.Scanner;

/**
 * Created by erlendvalle on 04.09.2017.
 */
public class Assignment3 {
	public static void main(String[] args) {
		int result = 0;
		int input = 0;

		Scanner scanner = new Scanner(System.in);
		System.out.print("Skriv inn et tall mellom 0 og 999: ");
		input = scanner.nextInt();
		if (input < 0 || input > 999) {
			main(args);
			System.out.println("We exited from main()");
		}
			int originalInput = input;

			result = input % 10;
			input = input / 10;

			result += input % 10;
			input = input / 10;

			result += input;

			System.out.println("Summen av siffer i " + originalInput + " er: " + result);
	}
}
