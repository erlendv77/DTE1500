package module2;

import java.util.Scanner;

/**
 * Created by erlendvalle on 04.09.2017.
 */
public class Assignment1 {
	public static void main(String[] args) {
		double weight = 0;
		double height = 0;
		double bmi = 0;

		Scanner scanner = new Scanner(System.in);

		System.out.println("Programmet beregner din BMI");

		System.out.print("Oppgi din vekt i kilogram: ");
		weight = scanner.nextDouble();

		System.out.print("Oppgi din høyde i meter: ");
		height = scanner.nextDouble();

		bmi = weight / Math.pow(height, 2);
		System.out.println("Din BMI er: " + bmi);
	}
}
