package module2;

import java.util.Scanner;

/**
 * Created by erlendvalle on 05.09.2017.
 */
public class Assignment4 {

	public static void main(String[] args) {
		double investmentValue = 0;
		double years = 0;
		double interest = 0;

		Scanner scanner = new Scanner(System.in);
		System.out.print("Oppgi investeringsverdi: ");
		investmentValue = scanner.nextDouble();

		System.out.print("Oppgi antall år: ");
		years = scanner.nextDouble();

		System.out.print("Oppgi årlig rente: ");
		interest =  scanner.nextDouble() / 1200.0;

		double futureInvestmentValue = investmentValue * Math.pow(interest + 1.0, years * 12.0);


		System.out.printf("Fremtidig investeringsverdi er: %.2f\n", futureInvestmentValue);
		System.out.printf("Mitt tall er %+10d,- ", 3);
	}

}
