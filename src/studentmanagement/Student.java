package studentmanagement;

public interface Student extends Person {

	public void setStudentNumber(String studentNumber);

	public String getStudentNumber();

}
