package studentmanagement;

public class StudentObject extends AbstractPerson implements Student {

	private String studentNumber;

	@Override
	public void setStudentNumber(String studentNumber) {
		this.studentNumber = studentNumber;
	}

	@Override
	public String getStudentNumber() {
		return studentNumber;
	}
}
