package studentmanagement;

import java.util.Date;

public abstract class AbstractPerson implements Person {

	private String name;
	private String address;
	private Date birth;
	private String phoneNumber;

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String getAddress() {
		return address;
	}

	@Override
	public void setBirth(Date birth) {
		this.birth = birth;
	}

	@Override
	public Date getBirth() {
		return birth;
	}

	@Override
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Override
	public String getPhoneNumber() {
		return phoneNumber;
	}
}
