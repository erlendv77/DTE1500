package studentmanagement;

public class StudentAssistantObject extends EmployeeObject implements StudentAssistant {
	private String studentNumber;


	@Override
	public void setStudentNumber(String studentNumber) {
		this.studentNumber = studentNumber;
	}

	@Override
	public String getStudentNumber() {
		return studentNumber;
	}
}
