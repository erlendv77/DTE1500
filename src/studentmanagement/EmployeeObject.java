package studentmanagement;

import java.util.Date;

public class EmployeeObject extends AbstractPerson implements Employee {
	private String employeeNumber;
	private String position;
	private Date employedDate;
	private int salary;


	@Override
	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	@Override
	public String getEmployeeNumber() {
		return employeeNumber;
	}

	@Override
	public void setPosition(String position) {
		this.position = position;
	}

	@Override
	public String getPosition() {
		return position;
	}

	@Override
	public void setEmployedDate(Date date) {
		this.employedDate = date;
	}

	@Override
	public Date getEmployedDate() {
		return employedDate;
	}

	@Override
	public void setSalary(int salary) {
		this.salary = salary;
	}

	@Override
	public int getSalary() {
		return salary;
	}
}
