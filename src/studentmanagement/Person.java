package studentmanagement;

import java.util.Date;

public interface Person {

	public void setName(String name);

	public String getName();

	public void setAddress(String address);

	public String getAddress();

	public void setBirth(Date birth);

	public Date getBirth();

	public void setPhoneNumber(String phoneNumber);

	public String getPhoneNumber();


}
