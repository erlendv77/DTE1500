package studentmanagement;

import java.util.Date;

public interface Employee {

	public void setEmployeeNumber(String employeeNumber);

	public String getEmployeeNumber();

	public void setPosition(String position);

	public String getPosition();

	public void setEmployedDate(Date date);

	public Date getEmployedDate();

	public void setSalary(int salary);

	public int getSalary();

}
