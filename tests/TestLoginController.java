import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import testing.ioc.LoginController;
import testing.ioc.PasswordEncrypter;
import testing.ioc.UserStorageMock;
import static org.junit.jupiter.api.Assertions.*;

public class TestLoginController {
	private LoginController loginController;

	@BeforeEach
	void setup() {
		loginController = new LoginController(new UserStorageMock(
				10,
				"erlend",
				"Erlend Valle",
				PasswordEncrypter.encrypt("secret")
		));
	}


	@Test
	void testUserAndPasswordIsCorrect() {
		assertTrue(loginController.login("erlend", "secret"));
	}

	@Test
	void testPasswordIsIncorrect() {
		assertFalse(loginController.login("erlend", "notsecret"));
	}

	@Test
	void testUserDoesntExist() {
		assertFalse(loginController.login("ola", "secret"));
	}



}
