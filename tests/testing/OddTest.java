package testing;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.*;

class OddTest {



	@Test
	void testIfOneIsOdd() {
		assertTrue(Odd.isOdd(1));
	}

	@Test
	void testIfThreeIsOdd() {
		assertTrue(Odd.isOdd(3));
	}


}