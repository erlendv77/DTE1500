import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import testing.ListStack;
import testing.Stack;

import static org.junit.jupiter.api.Assertions.*;


public class TestStack {

	// Uncomment next line to test ListStack implementation
	// ListStack stack;
	ListStack stack;

	@BeforeEach
	void initialize() {
		// Uncomment next line to test ListStack implementation
		// stack = new ListStack(10);
		stack = new ListStack(10);
	}


	@DisplayName("Test if size of stack is correct after creation")
	@Test
	void testSizeOfStackAfterCreation() {
		assertEquals(10, stack.size(), "Size of stack is incorrect");
	}

	@DisplayName("Elements is 0")
	@Test
	void testNumberOfElementsIsZeroAfterCreation() {
		assertEquals(0, stack.elements(), "Elements is not 0");
	}

	@Test
	void testNumberOfElementsIsCorrectAfterPush() {
		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.push(4);
		stack.push(5);

		assertEquals(5, stack.elements());
	}


	@Test
	void testNotAbleToPushToManyElements() {
		stack.push(1);
		stack.push(1);
		stack.push(1);
		stack.push(1);
		stack.push(1);
		stack.push(1);
		stack.push(1);
		stack.push(1);
		stack.push(1);
		stack.push(1);

		assertFalse(stack.push(11), "Able to push to many elements");
	}

	@Test
	void testPushReturnsTrue() {
		assertTrue(stack.push(1), "push return false");
	}

	@Test
	void testPeekReturnsTopElement() {
		stack.push(1);
		stack.push(2);
		stack.push(3);

		assertEquals(3, stack.peek(), "Peek doesnt return top element");
	}

	@Test
	void testPeekDoesntPop() {
		stack.push(1);
		stack.push(2);
		stack.push(3);

		stack.peek();

		assertEquals(3, stack.elements());
	}


	@Test
	void testPopPopsTopElement() {
		stack.push(1);
		stack.push(2);
		stack.push(3);

		int element = stack.pop();

		assertEquals(3, element, "Pop did not pop top element");
	}

	@Test
	void testElementsIsCorrectAfterPop() {
		stack.push(1);
		stack.push(2);
		stack.push(3);

		stack.pop();

		assertEquals(2, stack.elements(), "Size is incorrect");
	}

	@Test
	void testResizeToSmallerStack() {
		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.push(4);
		stack.push(5);
		stack.push(6);
		stack.push(7);
		stack.push(8);
		stack.push(9);
		stack.push(10);

		stack.resize(5);

		assertEquals(5, stack.size(), "Size is incorrect");

		assertEquals(5, stack.elements(), "Elements is incorrect");

		assertEquals(5, stack.peek(), "Peek is incorrect");

		assertEquals(5, stack.pop(), "Pop is incorrect");
	}


	@Test
	void testSizeIsCorrectAfterResizeToLarger() {
		stack.resize(20);

		assertEquals(20, stack.size(), "Size is incorrect");
	}

	@DisplayName("Test if resize to smaller stack is correct")
	@Test
	void testElementsIsCorrectAfterResizeToLarger() {
		stack.push(1);
		stack.push(1);
		stack.push(1);
		stack.push(1);
		stack.push(1);
		stack.push(1);
		stack.push(1);
		stack.push(1);
		stack.push(1);
		stack.push(1);

		stack.resize(20);

		assertEquals(10, stack.elements(), "Number of elements is incorrect");
	}

	@Test
	void testPeekIsCorrectAfterResizeToLarger() {
		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.push(4);
		stack.push(5);
		stack.push(6);
		stack.push(7);
		stack.push(8);
		stack.push(9);
		stack.push(10);

		stack.resize(20);

		assertEquals(10, stack.peek(), "Peek did not return correct element");
	}

	@Test
	void testPopIsCorrectAfterResize() {
		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.push(4);
		stack.push(5);
		stack.push(6);
		stack.push(7);
		stack.push(8);
		stack.push(9);
		stack.push(10);

		stack.resize(20);

		assertEquals(10, stack.pop(), "Pop did not pop correct element");
	}
}
